namespace EjemploFacturador.Migrations
{
    using EjemploFacturador.Models;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<EjemploFacturador.Models.FacturaContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(EjemploFacturador.Models.FacturaContext context)
        {
            var Productos = new List<Producto>
            {
                new Producto { Nombre = "Carro", Precio = 100.5m},
                new Producto { Nombre = "Moto", Precio = 110.5m},
                new Producto { Nombre = "Avion", Precio = 120.5m},
                new Producto { Nombre = "Helicoptero", Precio = 150.5m},

            };

            Productos.ForEach(s => context.Producto.AddOrUpdate(p => p.Nombre, s));
            context.SaveChanges();


            var facturas = new List<Factura>
            {
                new Factura { Cliente="Alberto", FechaCreacion= DateTime.Parse("1974-04-12")},
                new Factura { Cliente="Juan", FechaCreacion= DateTime.Parse("1974-04-12")},
            };

            facturas.ForEach(s => context.Factura.AddOrUpdate(p => p.Cliente, s));
            context.SaveChanges();


            var detalles = new List<DetalleFactura>
            {
                new DetalleFactura { ProductoID = context.Producto.Single(p => p.Nombre == "Carro").ID,
                                    PrecioUnitario = context.Producto.Single(p => p.Nombre == "Carro").Precio,
                                    Cantidad = 2,
                                    PrecioXCantidad = context.Producto.Single(p => p.Nombre == "Carro").Precio * 2,
                                    FacturaID = context.Factura.Single(f => f.Cliente == "Alberto").ID},

                new DetalleFactura { ProductoID = context.Producto.Single(p => p.Nombre == "Avion").ID,
                                    PrecioUnitario = context.Producto.Single(p => p.Nombre == "Avion").Precio,
                                    Cantidad = 1,
                                    PrecioXCantidad = context.Producto.Single(p => p.Nombre == "Avion").Precio * 1,
                                    FacturaID = context.Factura.Single(f => f.Cliente == "Alberto").ID},

                new DetalleFactura { ProductoID = context.Producto.Single(p => p.Nombre == "Helicoptero").ID,
                                    PrecioUnitario = context.Producto.Single(p => p.Nombre == "Helicoptero").Precio,
                                    Cantidad = 3,
                                    PrecioXCantidad = context.Producto.Single(p => p.Nombre == "Helicoptero").Precio * 3,
                                    FacturaID = context.Factura.Single(f => f.Cliente == "Juan").ID},
            };

            foreach (DetalleFactura d in detalles)
            {
                var detalleenDataBase = context.DetalleFactura.Where(
                    f =>
                         f.Factura.ID == d.FacturaID &&
                         f.Producto.ID == d.ProductoID).SingleOrDefault();

                if (detalleenDataBase == null)
                {
                    context.DetalleFactura.Add(d);
                }
            }
            context.SaveChanges();

        }
    }
}
