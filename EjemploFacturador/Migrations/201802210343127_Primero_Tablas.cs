namespace EjemploFacturador.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Primero_Tablas : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.DetalleFactura",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        FacturaID = c.Int(nullable: false),
                        ProductoID = c.Int(nullable: false),
                        Cantidad = c.Int(nullable: false),
                        PrecioUnitario = c.Decimal(nullable: false, precision: 18, scale: 2),
                        PrecioXCantidad = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Facturas", t => t.FacturaID, cascadeDelete: true)
                .ForeignKey("dbo.Productos", t => t.ProductoID, cascadeDelete: true)
                .Index(t => t.FacturaID)
                .Index(t => t.ProductoID);
            
            CreateTable(
                "dbo.Facturas",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Cliente = c.String(),
                        Total = c.Decimal(nullable: false, precision: 18, scale: 2),
                        FechaCreacion = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Productos",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Nombre = c.String(),
                        Precio = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.DetalleFactura", "ProductoID", "dbo.Productos");
            DropForeignKey("dbo.DetalleFactura", "FacturaID", "dbo.Facturas");
            DropIndex("dbo.DetalleFactura", new[] { "ProductoID" });
            DropIndex("dbo.DetalleFactura", new[] { "FacturaID" });
            DropTable("dbo.Productos");
            DropTable("dbo.Facturas");
            DropTable("dbo.DetalleFactura");
        }
    }
}
