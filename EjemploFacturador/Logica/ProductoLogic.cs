﻿using EjemploFacturador.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EjemploFacturador.Logica
{
    public class ProductoLogic
    {
        public List<Producto> Buscar(string nombre)
        {
            using (var context = new FacturaContext())
            {
                context.Configuration.LazyLoadingEnabled = false;
                context.Configuration.ProxyCreationEnabled = false;

                var productos = context.Producto.OrderBy(x => x.Nombre)
                                        .Where(x => x.Nombre.Contains(nombre))
                                        .ToList();

                return productos;
            }
        }
    }
}