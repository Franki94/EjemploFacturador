﻿using EjemploFacturador.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace EjemploFacturador.Logica
{
    public class FacturaLogic
    {
        public bool Registrar(Factura comprobante)
        {
            try
            {
                using (var context = new FacturaContext())
                {
                    context.Entry(comprobante).State = EntityState.Added;
                    context.SaveChanges();
                }
            }
            catch (Exception)
            {
                return false;
            }

            return true;
        }

        public Factura Obtener(int id)
        {
            using (var context = new FacturaContext())
            {
                // Esta consulta incluye el detalle del comprobante, y el producto que tiene cada comprobante. Me refiero a sus relaciones
                return context.Factura.Include(x => x.DetalleFactura.Select(y => y.Producto))
                                          .Where(x => x.ID == id)
                                          .SingleOrDefault();
            }
        }

        public List<Factura> Listar()
        {
            using (var context = new FacturaContext())
            {
                return context.Factura.OrderByDescending(x => x.FechaCreacion)
                                          .ToList();
            }
        }
    }
}