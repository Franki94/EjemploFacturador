﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace EjemploFacturador.Models
{ 
    [Table(name: "Productos")]
    public class Producto
    {
        public Producto()
        {
            DetallesFactura = new List<DetalleFactura>(); 
        }
        public int ID { get; set; }

        public string Nombre { get; set; }

        public decimal Precio { get; set; }

        public virtual List<DetalleFactura> DetallesFactura { get; set; }
    }
}