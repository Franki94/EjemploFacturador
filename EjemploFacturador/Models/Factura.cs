﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace EjemploFacturador.Models
{
    [Table(name: "Facturas")]
    public class Factura
    {
        public Factura()
        {
            DetalleFactura = new List<DetalleFactura>();
        }
        public int ID { get; set; }

        public string Cliente { get; set; }

        public decimal Total { get; set; }

        public DateTime FechaCreacion { get; set; }

        public virtual List<DetalleFactura> DetalleFactura { get; set; }
    }
}