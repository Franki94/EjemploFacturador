﻿using System.ComponentModel.DataAnnotations.Schema;

namespace EjemploFacturador.Models
{
    [Table(name: "DetalleFactura")]
    public class DetalleFactura
    {
        public int ID { get; set; }

        public int FacturaID { get; set; }

        public int ProductoID { get; set; }

        public int Cantidad { get; set; }

        public decimal PrecioUnitario { get; set; }

        public decimal PrecioXCantidad { get; set; }

        public virtual Factura Factura { get; set; }

        public virtual Producto Producto { get; set; }
    }
}