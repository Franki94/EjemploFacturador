﻿using EjemploFacturador.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EjemploFacturador.ModelsView
{
    public class FacturaViewModel
    {
            #region Cabecera
            public string Cliente { get; set; }
            public int CabeceraProductoId { get; set; }
            public string CabeceraProductoNombre { get; set; }
            public int CabeceraProductoCantidad { get; set; }
            public decimal CabeceraProductoPrecio { get; set; }
            #endregion

            #region Contenido
            public List<FacturaDetalleViewModel> FacturaDetalle { get; set; }
            #endregion

            #region Pie
            public decimal Total()
            {
                return FacturaDetalle.Sum(x => x.Total());
            }
            public DateTime Creado { get; set; }
            #endregion

            public FacturaViewModel()
            {
                FacturaDetalle = new List<FacturaDetalleViewModel>();
                Refrescar();
            }

            public void Refrescar()
            {
                CabeceraProductoId = 0;
                CabeceraProductoNombre = null;
                CabeceraProductoCantidad = 1;
                CabeceraProductoPrecio = 0;
            }

            public bool SeAgregoUnProductoValido()
            {
                return !(CabeceraProductoId == 0 || string.IsNullOrEmpty(CabeceraProductoNombre) || CabeceraProductoCantidad == 0 || CabeceraProductoPrecio == 0);
            }

            public bool ExisteEnDetalle(int ProductoId)
            {
                return FacturaDetalle.Any(x => x.IdProducto == ProductoId);
            }

            public void RetirarItemDeDetalle()
            {
                if (FacturaDetalle.Count > 0)
                {
                    var detalleARetirar = FacturaDetalle.Where(x => x.Retirar)
                                                            .SingleOrDefault();

                    FacturaDetalle.Remove(detalleARetirar);
                }
            }

            public void AgregarItemADetalle()
            {
                FacturaDetalle.Add(new FacturaDetalleViewModel
                {
                    IdProducto = CabeceraProductoId,
                    NombreProducto = CabeceraProductoNombre,
                    PrecioUnitario = CabeceraProductoPrecio,
                    Cantidad = CabeceraProductoCantidad,
                });

                Refrescar();
            }

            public Factura ToModel()
            {
                var comprobante = new Factura();
                comprobante.Cliente = this.Cliente;
                comprobante.FechaCreacion = DateTime.Now;
                comprobante.Total = this.Total();
               

                foreach (var d in FacturaDetalle)
                {
                    comprobante.DetalleFactura.Add(new DetalleFactura
                    {
                        ProductoID = d.IdProducto,
                        PrecioXCantidad = d.Total(),
                        PrecioUnitario = d.PrecioUnitario,
                        Cantidad = d.Cantidad,            
                    });
                }

                return comprobante;
            }
        }
}