﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EjemploFacturador.ModelsView
{
    public class FacturaDetalleViewModel
    {
        public int IdProducto { get; set; }
        public string NombreProducto { get; set; }

        public int Cantidad { get; set; }
        public decimal PrecioUnitario { get; set; }
        public bool Retirar { get; set; }

        public decimal Total()
        {
            return Cantidad * PrecioUnitario;
        }
    }
}